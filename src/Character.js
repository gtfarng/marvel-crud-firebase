import React, { Component } from 'react';
import firebase from 'firebase';
import logo from './avenger.svg';

export default class Character extends Component {

    constructor(props) {
        super(props)
        this.state = {
            members:
                [{ id: 1, name: 'Robert John Downey, Jr.', character: 'Iron Man/Tony Stark' },
                { id: 2, name: 'Christopher Robert Evans', character: 'Captain America/Steve Rogers' },
                { id: 3, name: 'Scarlett Johansson', character: 'Black Widow/Natasha Romanoff' },
                { id: 4, name: 'Elizabeth Olsen', character: 'Scarlet Witch/Wanda Maximoff' },
                { id: 5, name: 'Tom Holland', character: 'Spider-Man/Peter Parker' }]
            ,
            member: ''
        }

        let config = {
            apiKey: "AIzaSyC5zq7zIN0ZszrY0CVGcRR6ujqV4s9mvcY",
            authDomain: "marvel-crud.firebaseapp.com",
            databaseURL: "https://marvel-crud.firebaseio.com",
            projectId: "marvel-crud",
            storageBucket: "",
            messagingSenderId: "194487226339"
        };

        if (firebase.apps.length === 0) firebase.initializeApp(config)

        console.log('firebase: ', firebase.database())
        console.log('firebase: ', firebase.app().name)

        let myapp = firebase.database().ref('/');
        let tasksChild = myapp.child('/taskTable')
        tasksChild.remove()
        tasksChild.set({ members: this.state.members })
        myapp.on('value', snapshot => {
            console.log('member-0: ', snapshot.val())
        });
    }

    removeMember = (id) => {
        let array = [...this.state.members]; // make a separate copy of the array
        let index = array.findIndex((member) => member.id === id)
        array.splice(index, 1)
        this.setState({ members: array })
        console.log('update states members:', this.state.members)

        let tasksChild = firebase.database()
            .ref('/')
            .child('/taskTable/members/' + (id - 1))
        tasksChild.remove()
            .then(() => console.log("Remove success: "))
            .catch((err) => console.log("Remove failed: " + err))
    }

    addMember = () => {
        let lastItem = this.state.members[this.state.members.length - 1]
        let newTask = { id: lastItem.id + 1, name: this.state.member, character: this.state.character }
        this.setState({ members: [...this.state.members, newTask] })
        console.log("Add button")
        let tasksChild = firebase.database()
            .ref('/')
            .child('/taskTable/members/' + lastItem.id)
        tasksChild.set(newTask)
            .then(() => console.log("Add successfully: "))
            .catch((err) => console.log("Remove failed: " + err))
    }

    editMember = (id) => {
        let index = this.state.members.findIndex((member) => member.id === id)
        this.setState({ member: this.state.members[index].name, character: this.state.members[index].character })
        console.log("Get button")
    }
    

    updateMember = (id) => {
        console.log('update states members sss:', this.state.members)
        let array = [...this.state.members]
      //  let arrays = [...this.state.members]
        let index = array.findIndex((member) => member.id === id)
      
        array[index].name = this.state.member
        array[index].character = this.state.character
        
       // arrays[index].character = this.state.member
        this.setState({ members: array })
        console.log("update button")
        console.log("state test:",this.state.member)
        let tasksChild = firebase.database()
            .ref('/')
            .child('/taskTable/members/' + (id - 1))
        tasksChild.set({ id: array[index].id, name: this.state.member, character: this.state.character })
            .then(() => console.log("Update successfully: "))
            .catch((err) => console.log("Update failed:" + err))
    }

    handleChangeName = (e) => this.setState({ member: e.target.value },
        console.log("name: ", this.state.member)
    )

    handleChangesCharacter = (e) => this.setState({ character: e.target.value },
        console.log("charactor: ", this.state.character)
    )

    renderMembers = () => {
        if (this.state.members.length !== 0)
            return this.state.members.map((member, index) => {
                return (
                    <tr key={index}>
                        <td>{member.id}.</td>&nbsp;
                       <td><strong>Name:</strong> {member.name}<strong> &nbsp;&nbsp;Character:</strong> {member.character}</td>&nbsp; &nbsp; &nbsp;
                       <td>
                            <button class="btn btn-outline-success" onClick={() => this.editMember(member.id)}> GET</button>
                        </td>&nbsp;
                       <td>
                            <button class="btn btn-outline-warning" onClick={() => this.updateMember(member.id)}> UPDATE</button>
                        </td>&nbsp;
                       <td>
                            <button class="btn btn-outline-danger" onClick={() => this.removeMember(member.id)}> DELETE</button>
                        </td>
                    </tr>)
            })
    }

    render() {
        return (
            <div style={{ margin: '40px' }} align="center">

                <h2><strong>Character of MARVEL Universe (CRUD)</strong> </h2><br /><br />
                <img src={logo} alt="logo" width="35%" /><br /><br /><br /><br />

                <input type="text" placeholder="Name..." name="member" onChange={this.handleChangeName} value={this.state.member} />&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="text" placeholder="Character..." name="character" onChange={this.handleChangesCharacter} value={this.state.character} />&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn btn-outline-primary" onClick={this.addMember}>ADD</button>
                <br /><br /> <br />
                <h2>Render Avengers Team</h2>
                <table >
                    { /*<thead>
                   <tr>
                       <th >ID</th>&nbsp;
                       <th>member</th>&nbsp;
                       <th colSpan={3}>Action</th>&nbsp; 
                   </tr>
                   </thead>
                  */ }

                    <br />

                    <tbody>
                        {this.renderMembers()}
                    </tbody>
                </table>
            </div>
        );
    }
}

